package com.devcamp.midqualification.deparmentemployee.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;


@Entity
@Table(name = "departments")
public class Department {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotEmpty(message = "Nhập mã phòng ban")
    @Column(name = "department_code", unique = true)
    private String departmentCode;

    @NotEmpty(message = "Nhập tên phòng ban")
    @Column(name = "department_name")
    private String departmentName;

    @NotEmpty(message = "Nhập nghiệp vụ phòng ban")
    private String major;

    @NotEmpty(message = "Nhập lời giới thiệu cho phòng ban")
    private String introduce;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "department")
    private Set<Employee> employees;

    public Department() {
    }

    public Department(@NotEmpty(message = "Nhập mã phòng ban") String departmentCode,
            @NotEmpty(message = "Nhập tên phòng ban") String departmentName,
            @NotEmpty(message = "Nhập nghiệp vụ phòng ban") String major,
            @NotEmpty(message = "Nhập lời giới thiệu cho phòng ban") String introduce, Set<Employee> employees) {
        this.departmentCode = departmentCode;
        this.departmentName = departmentName;
        this.major = major;
        this.introduce = introduce;
        this.employees = employees;
    }

    public long getId() {
        return id;
    }


    public void setId(long id) {
        this.id = id;
    }


    public String getDepartmentCode() {
        return departmentCode;
    }


    public void setDepartmentCode(String departmentCode) {
        this.departmentCode = departmentCode;
    }


    public String getDepartmentName() {
        return departmentName;
    }


    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }


    public String getMajor() {
        return major;
    }


    public void setMajor(String major) {
        this.major = major;
    }


    public String getIntroduce() {
        return introduce;
    }


    public void setIntroduce(String introduce) {
        this.introduce = introduce;
    }


    public Set<Employee> getEmployees() {
        return employees;
    }


    public void setEmployees(Set<Employee> employees) {
        this.employees = employees;
    }

}
