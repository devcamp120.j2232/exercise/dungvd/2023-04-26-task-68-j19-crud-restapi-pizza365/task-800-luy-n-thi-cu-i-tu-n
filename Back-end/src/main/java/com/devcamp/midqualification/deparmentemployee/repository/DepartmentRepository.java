package com.devcamp.midqualification.deparmentemployee.repository;


import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.midqualification.deparmentemployee.model.Department;

public interface DepartmentRepository extends JpaRepository<Department, Long>{

}
