-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 01, 2023 at 03:39 PM
-- Server version: 10.4.27-MariaDB
-- PHP Version: 8.0.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `department_employee`
--

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE `departments` (
  `id` bigint(20) NOT NULL,
  `department_code` varchar(255) DEFAULT NULL,
  `department_name` varchar(255) DEFAULT NULL,
  `introduce` varchar(255) DEFAULT NULL,
  `major` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`id`, `department_code`, `department_name`, `introduce`, `major`) VALUES
(17, 'CB', 'Cái Bang', 'Ăn mày', 'Ăn mày'),
(18, 'ĐT', 'Đại lý đoàn thị', 'Đại lý', 'Đại lý'),
(19, 'TL', 'Thiếu Lâm', 'Thiếu lâm chính tông', 'Thiếu lâm chính tông'),
(20, 'MG', 'Minh Giáo', 'Tà phái', 'Tà phái'),
(25, 'TN', 'Thiên nhẫn', 'Tà Pháii', 'Tà Phái');

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `id` bigint(20) NOT NULL,
  `date_of_birth` date DEFAULT NULL,
  `employee_code` varchar(255) DEFAULT NULL,
  `employee_name` varchar(255) DEFAULT NULL,
  `gender` char(1) NOT NULL,
  `position` varchar(255) DEFAULT NULL,
  `department_id` bigint(20) NOT NULL,
  `phone_number` varchar(10) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`id`, `date_of_birth`, `employee_code`, `employee_name`, `gender`, `position`, `department_id`, `phone_number`, `address`) VALUES
(1, '2023-04-02', 'TP', 'Tiêu Phong', 'm', 'Bang Chủ', 17, '0917354998', 'Nước Liêu'),
(33, '1998-07-10', 'TVK', 'Trương Vô Kỵ', 'm', 'Giáo chủ', 20, '0917354990', NULL),
(35, '2002-02-17', 'DD', 'Đoàn Dự', 'm', 'Vua', 18, '0961701728', NULL),
(39, '1998-03-31', 'BDDM', 'Bồ Đề Đạt Ma', 'm', 'Sư tổ', 19, '0961701729', NULL),
(49, '2023-04-04', 'HD', 'Hoàng Dung', 'm', 'Bang Chủ', 17, '0917354962', 'Đại Tống'),
(54, '2023-04-03', 'HTC', 'Hồng Thất Công', 'm', 'Bang Chủ', 17, '0917354992', 'Đại Tống'),
(58, '2002-02-15', 'DCT', 'Đoàn Chính Thuần', 'm', 'Cha vua', 18, '0961701720', NULL),
(61, '2002-05-17', 'HT', 'Huyền Từ', 'm', 'Phương Trượng', 19, '0213468744', NULL),
(62, '1990-02-01', 'TT', 'Tạ Tốn', 'm', 'Giáo chủ', 20, '5555555555', NULL),
(63, '2002-02-14', 'DCM', 'Đoàn Chính Minh', 'm', 'Cha vua', 18, '0961701722', NULL),
(64, '1967-12-08', 'HDS', 'Hoàng Dược Sư', 'm', 'Đảo chủ', 17, '0136475782', 'Đảo Đào Hoa');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UK_89g8qie2y696a3tarmty43sq9` (`department_code`);

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UK_etqhw9qqnad1kyjq3ks1glw8x` (`employee_code`),
  ADD KEY `FKgy4qe3dnqrm3ktd76sxp7n4c2` (`department_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `departments`
--
ALTER TABLE `departments`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `employees`
--
ALTER TABLE `employees`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `employees`
--
ALTER TABLE `employees`
  ADD CONSTRAINT `FKgy4qe3dnqrm3ktd76sxp7n4c2` FOREIGN KEY (`department_id`) REFERENCES `departments` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
